#!/bin/bash
usage()
{
cat | less << EOT
DOMYSLNA NAZWA
  install.sh

OPIS
  Pobiera niezbedne pakiety potrzebne do uruchomienia linuxcnc, nastepnie zmienia potem kompiluje linuxcnc i ustawia konto uzytkownika. Skrypt nalezy wykonywać jako root.
  
OPCJE:
  -r
  instalacja repozytoriow
  
  -p 
  instalacja niezbednych pakietow
    
  -k kernel
  instaluje powloke rt ze strony toromatic.pl
    
  -c cnc
  pobiera linuxcnc z git i ustawia na wersje 2.7 , po czym kompiluje zrodla

  -a auto login
  ustawia automatyczne zalogowanie uzytkownika x
    
  -u user space
  konfiguruje konta uzytkownika x
    
    -l konfiguracja panelu lxpanel dla roota
  dodaje pliki konfiguracyjne lxpanelu tak aby przypominal gnome2, ma na celu ulatwic prace administratorowi - pasek domyslnie jest czarny zarowno ten u gory jak i na dole
    
  domyslnie 
  wykonuje -rpkcaul

AUTOR
  Andrzej Bartkowski <be.worse@gmail.com>
  
WERSJA
  0.52a z dnia 05 październik 2016 (aktualizacja listy do zrobienia czerwiec 2017)
  
DO ZROBIENIA
  - środowisko admina
  - ściągnąć kilka tapet
  - zmienic sposob montowania pendriva(ów)
  + przetestować na prawdziwej maszynie
  + przetestować na zmodyfikowanej instalce debiania
  - dodać logi, które będą łatwe do przeczytania
EOT
}

#-------- ZMIENNE --------#

#nazwa uzytkownika
USER="x" 

#sciezka do katalog uzytkownika
HOME="/home/"$USER"/"

#nazwa katalogu
LNCMKDIR="realtime"

#sciezka do linuxcnc
LNCDIR=$HOME$LNCMKDIR"/linuxcnc"

#zmiana uzytkownika
SUSER="sudo -H -u "$USER

#-------- FUNKCJE INSTALACYJNE --------#

installrepository()
{
echo "----------------------------------------------"
echo "tworzenie repozytoriow"
echo "----------------------------------------------" 

#utworzenie kopii zapasowej
cp /etc/apt/sources.list /etc/apt/sources.list.backup

#utworzenie wlasciwego repozytroium
rm /etc/apt/sources.list

cat <<EOT >> /etc/apt/sources.list
deb http://ftp.pl.debian.org/debian/ jessie main
deb-src http://ftp.pl.debian.org/debian/ jessie main

deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main

# jessie-updates, previously known as 'volatile'
deb http://ftp.pl.debian.org/debian/ jessie-updates main
deb-src http://ftp.pl.debian.org/debian/ jessie-updates main
EOT

echo "----------------------------------------------"
echo "tworzenie repozytoriow zakonczone"
echo "----------------------------------------------" 
}

installpackages() #instalacja niezbednych komponentow
{ 
echo "----------------------------------------------"
echo "sprawdzanie niezbednych pakietow"
echo "----------------------------------------------"

apt-get update #aktualizacja repozytoriow

#srodowisko graficzne
apt-get install -y xserver-xorg xinit xterm #potrzebne do uruchomienia X11
#apt-get install -y conky #monitor systemu zgodny z X11
apt-get install -y openbox #menadzer okien do X11
apt-get install -y nitrogen #ustawianie tapety
apt-get install -y lxpanel #pasek

#programy
apt-get install -y synaptic
apt-get install -y gdebi #lepszy menadzer pakietow
apt-get install -y --no-install-recommends geany #edytor tekstowy
apt-get install -y --no-install-recommends mc #menedzer plikow
apt-get install -y --no-install-recommends htop nmon iotop #informacje o systemie
apt-get install -y --no-install-recommends git
apt-get install -y --no-install-recommends kdiff3 kompare #porownywanie plikow
apt-get install -y python-qt4-gl python-qt4 #potrzbene do programu
apt-get install -y pcmanfm #maly menadzer plikow
apt-get install -y usbmount #automatyczne montowanie usb
apt-get install -y xarchiver #rozpakowywanie archiwow
apt-get install -y gnome-terminal #fajny terminal
apt-get install -y iceweasel #przegladarka 
apt-get install -y evince #do ewentualnego przegladnia dokumentacji


#narzedzia do dyskow
apt-get install -y gparted
apt-get install -y gnome-disk-utility 

#GUI
apt-get install -y --no-install-recommends qt4-designer #tworzednie widgetow qt4
apt-get install -y --no-install-recommends rcconf #program do konfiguracji autostartu
apt-get install -y --no-install-recommends pv #pipe viewer
#apt-get install -y --no-install-recommends netsurf #przegladarka internetowa
apt-get install -y --no-install-recommends lshw

#linuxcnc
apt-get install -y linux-headers-$(uname -r)  #do wirtualnej maszyny
apt-get install -y --no-install-recommends autoconf pkg-config libudev-dev libusb-1.0-0-dev libglib2.0-dev libgtk2.0-dev build-essential tcl8.6-dev tk8.6-dev libxaw7-dev libncurses-dev libreadline-gplv2-dev asciidoc source-highlight xsltproc groff python-tk libglu1-mesa-dev libgl1-mesa-dev graphviz w3c-linkchecker bwidget libtk-img tclx libusb-1.0-0-dev libmodbus-dev libboost-python-dev pyqt4-dev-tools python-qt4-gl gettext python-gtk2 python-opengl
apt-get install -y --no-install-recommends python-gtk2 python-opengl   #brakujace biblioteki
apt-get install -y sudo fuse

echo "----------------------------------------------"
echo "sprawdzanie niezbednych pakietow zakonczone"
echo "----------------------------------------------"
}

installlinuxcnc() #instalacja linuxcnc
{
echo "----------------------------------------------"
echo "instalacja linuxcnc"
echo "----------------------------------------------"
  
#przejscie do katalogu domowego uzytkownika
cd $HOME

#stworzenie katalogu jako user
$SUSER mkdir -pv $LNCMKDIR

#wejscie do katalogu
cd $LNCMKDIR

#pobranie linuxcnc z gita
$SUSER git clone git://git.linuxcnc.org/git/linuxcnc.git

#ustawienie wersji linuxcnc
cd linuxcnc
$SUSER git checkout 2.7

#todo instalacja GUI

cd src
$SUSER ./autogen.sh
$SUSER ./configure --with-realtime=uspace
$SUSER make
make setuid

cd ..
cd scripts
$SUSER chmod +x linuxcnc
echo "----------------------------------------------"
echo "instalacja linuxcnc zakonczona"
echo "----------------------------------------------"
}

installkernel() #instalacja jadra
{
echo "----------------------------------------------"
echo "instalacja jadra rt"
echo "----------------------------------------------"

cd $HOME
#pobranie
echo "Pobieranie ..."
wget --user-agent="ok" http://toromatic.pl/files/linux-firmware-image-3.18.13-rt10mah+_3.18.13-rt10mah+-2_i386.deb
wget --user-agent="ok" http://toromatic.pl/files/linux-headers-3.18.13-rt10mah+_3.18.13-rt10mah+-2_i386.deb
wget --user-agent="ok" http://toromatic.pl/files/linux-image-3.18.13-rt10mah+_3.18.13-rt10mah+-2_i386.deb
wget --user-agent="ok" http://toromatic.pl/files/linux-libc-dev_3.18.13-rt10mah+-2_i386.deb

#instalacja
echo "Instalacja ..."
ls | grep deb |xargs dpkg -i

#usuniecie
echo "Usuwanie plikow ..."
rm linux-firmware-image.deb linux-headers.deb linux-image.deb linux-libc.deb  

#aktualizacja gruba
echo "Aktualizacja grub ..."
update-grub

echo "----------------------------------------------"
echo "instalacja jadra rt zakonczona"
echo "----------------------------------------------"
}

#-------- FUNKCJE KONFIGURACYJNE --------#
setautologin() #autologowanie
{
AUTODIR="/etc/systemd/system/getty@tty1.service.d"
AUTODIRFILE=$AUTODIR"/autologin.conf"
  
echo "----------------------------------------------"
echo "Ustawienie opcji autologowania"
echo "----------------------------------------------" 

#utworzenie katalogu uslugi dla tty1
mkdir -pv $AUTODIR

#utworzenie uslugi
cat <<EOT >> $AUTODIRFILE
[Service]
ExecStart=
EOT
echo "ExecStart=-/sbin/agetty --autologin "$USER" --noclear %I 38400 linux" >> $AUTODIRFILE

#dodanie startx po zalagowaniu sie uzytkownika na tty1
cat <<EOT >> $HOME"/.bashrc"

if [ -z "\$DISPLAY" ] && [ "\$(fgconsole)" -eq 1 ]; then
  exec startx
fi
EOT

echo "----------------------------------------------"
echo "Ustawienie opcji autologowania zakonczone" 
echo "----------------------------------------------" 
}

configuserspace() #konifugre openboxa
{
OBNDIR=$HOME".config/openbox/"
BACKDIR=$HOME"/Obrazy/"
  
echo "----------------------------------------------"
echo "konfiguracja openboxa"
echo "----------------------------------------------"

cd $HOME

#utworzenie katalogu konfiguracyjnego open-boxa
echo "tworzenie katalogow..."
mkdir -pv $OBNDIR

echo "Nadawanie uztykownikowi "$USER" uprawnien do ponownego uruchomienia oraz wylaczenia komputera"
chmod a+s /sbin/shutdown
chmod a+s /sbin/reboot

echo "Tworzenie ustawien domyslny ..."

#ustawienie openbox-session jako domyslne srodowisko graficzne dla startx
echo "openbox-session &" > $HOME".initrc"

#dodanie linuxcnc do autostartu openboxa
echo $LNCDIR"/scripts/linuxcnc -l &" > $OBNDIR"autostart"

#zapisywanie konfiguracji menu openboxa
cat <<EOT > $OBNDIR"menu.xml"
<?xml version="1.0" encoding="UTF-8"?>

<openbox_menu xmlns="http://openbox.org/"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://openbox.org/
                file:///usr/share/openbox/menu.xsd">

<menu id="root-menu" label="Openbox 3">
  <item label="Terminal">
    <action name="Execute"><execute>x-terminal-emulator</execute></action>
  </item>
  <separator />
  <item label="Linuxcnc">
EOT

#dodanie linux CNC
echo '    <action name="Execute"><execute>'$LNCDIR"/scripts/linuxcnc -l</execute></action>\n   </item>" >> $OBNDIR"menu.xml"

#ustaw tapete
echo '  <separator />\n   <item label="Ustaw tapete">\n    <action name="Execute"><execute>nitrogen '$HOME'Obrazy</execute></action>\n  </item>' >> $OBNDIR"menu.xml"

#dodanie pozostalych elemtow
cat <<EOT >> $OBNDIR"menu.xml"
  <separator />
  <item label="Przegladarka WWW">
    <action name="Execute"><execute>x-www-browser</execute></action>
  </item>
  <separator />
  <item label="Menadzer plikow">
    <action name="Execute"><execute>pcmanfm</execute></action>
  </item>
  <separator />
  <item label="Uruchom ponownie">
    <action name="Execute"><execute>/sbin/reboot</execute></action>
  </item>
  <separator />
  <item label="Wylacz">
    <action name="Execute"><execute>/sbin/shutdown -h now</execute></action>
  </item>
</menu>
</openbox_menu>
EOT


#zapisywanie konfiguracji menu openboxa
cat <<EOT > $OBNDIR"rc.xml"
<?xml version="1.0" encoding="UTF-8"?>

<!-- Do not edit this file, it will be overwritten on install.
        Copy the file to $HOME/.config/openbox/ instead. -->

<openbox_config xmlns="http://openbox.org/3.4/rc"
    xmlns:xi="http://www.w3.org/2001/XInclude">

<resistance>
  <strength>10</strength>
  <screen_edge_strength>20</screen_edge_strength>
</resistance>

<focus>
  <focusNew>yes</focusNew>
  <!-- always try to focus new windows when they appear. other rules do
       apply -->
  <followMouse>no</followMouse>
  <!-- move focus to a window when you move the mouse into it -->
  <focusLast>yes</focusLast>
  <!-- focus the last used window when changing desktops, instead of the one
       under the mouse pointer. when followMouse is enabled -->
  <underMouse>no</underMouse>
  <!-- move focus under the mouse, even when the mouse is not moving -->
  <focusDelay>200</focusDelay>
  <!-- when followMouse is enabled, the mouse must be inside the window for
       this many milliseconds (1000 = 1 sec) before moving focus to it -->
  <raiseOnFocus>no</raiseOnFocus>
  <!-- when followMouse is enabled, and a window is given focus by moving the
       mouse into it, also raise the window -->
</focus>

<placement>
  <policy>Smart</policy>
  <!-- 'Smart' or 'UnderMouse' -->
  <center>yes</center>
  <!-- whether to place windows in the center of the free area found or
       the top left corner -->
  <monitor>Primary</monitor>
  <!-- with Smart placement on a multi-monitor system, try to place new windows
       on: 'Any' - any monitor, 'Mouse' - where the mouse is, 'Active' - where
       the active window is, 'Primary' - only on the primary monitor -->
  <primaryMonitor>1</primaryMonitor>
  <!-- The monitor where Openbox should place popup dialogs such as the
       focus cycling popup, or the desktop switch popup.  It can be an index
       from 1, specifying a particular monitor.  Or it can be one of the
       following: 'Mouse' - where the mouse is, or
                  'Active' - where the active window is -->
</placement>

<theme>
  <name>Clearlooks</name>
  <titleLayout>NLIMC</titleLayout>
  <!--
      available characters are NDSLIMC, each can occur at most once.
      N: window icon
      L: window label (AKA title).
      I: iconify
      M: maximize
      C: close
      S: shade (roll up/down)
      D: omnipresent (on all desktops).
  -->
  <keepBorder>yes</keepBorder>
  <animateIconify>yes</animateIconify>
  <font place="ActiveWindow">
    <name>sans</name>
    <size>8</size>
    <!-- font size in points -->
    <weight>bold</weight>
    <!-- 'bold' or 'normal' -->
    <slant>normal</slant>
    <!-- 'italic' or 'normal' -->
  </font>
  <font place="InactiveWindow">
    <name>sans</name>
    <size>8</size>
    <!-- font size in points -->
    <weight>bold</weight>
    <!-- 'bold' or 'normal' -->
    <slant>normal</slant>
    <!-- 'italic' or 'normal' -->
  </font>
  <font place="MenuHeader">
    <name>sans</name>
    <size>9</size>
    <!-- font size in points -->
    <weight>normal</weight>
    <!-- 'bold' or 'normal' -->
    <slant>normal</slant>
    <!-- 'italic' or 'normal' -->
  </font>
  <font place="MenuItem">
    <name>sans</name>
    <size>9</size>
    <!-- font size in points -->
    <weight>normal</weight>
    <!-- 'bold' or 'normal' -->
    <slant>normal</slant>
    <!-- 'italic' or 'normal' -->
  </font>
  <font place="ActiveOnScreenDisplay">
    <name>sans</name>
    <size>9</size>
    <!-- font size in points -->
    <weight>bold</weight>
    <!-- 'bold' or 'normal' -->
    <slant>normal</slant>
    <!-- 'italic' or 'normal' -->
  </font>
  <font place="InactiveOnScreenDisplay">
    <name>sans</name>
    <size>9</size>
    <!-- font size in points -->
    <weight>bold</weight>
    <!-- 'bold' or 'normal' -->
    <slant>normal</slant>
    <!-- 'italic' or 'normal' -->
  </font>
</theme>

<desktops>
  <!-- this stuff is only used at startup, pagers allow you to change them
       during a session

       these are default values to use when other ones are not already set
       by other applications, or saved in your session

       use obconf if you want to change these without having to log out
       and back in -->
  <number>1</number>
  <firstdesk>1</firstdesk>
  <names>
    <!-- set names up here if you want to, like this:
    <name>desktop 1</name>
    <name>desktop 2</name>
    -->
  </names>
  <popupTime>875</popupTime>
  <!-- The number of milliseconds to show the popup for when switching
       desktops.  Set this to 0 to disable the popup. -->
</desktops>

<resize>
  <drawContents>yes</drawContents>
  <popupShow>Nonpixel</popupShow>
  <!-- 'Always', 'Never', or 'Nonpixel' (xterms and such) -->
  <popupPosition>Center</popupPosition>
  <!-- 'Center', 'Top', or 'Fixed' -->
  <popupFixedPosition>
    <!-- these are used if popupPosition is set to 'Fixed' -->

    <x>10</x>
    <!-- positive number for distance from left edge, negative number for
         distance from right edge, or 'Center' -->
    <y>10</y>
    <!-- positive number for distance from top edge, negative number for
         distance from bottom edge, or 'Center' -->
  </popupFixedPosition>
</resize>

<!-- You can reserve a portion of your screen where windows will not cover when
     they are maximized, or when they are initially placed.
     Many programs reserve space automatically, but you can use this in other
     cases. -->
<margins>
  <top>0</top>
  <bottom>0</bottom>
  <left>0</left>
  <right>0</right>
</margins>

<dock>
  <position>TopLeft</position>
  <!-- (Top|Bottom)(Left|Right|)|Top|Bottom|Left|Right|Floating -->
  <floatingX>0</floatingX>
  <floatingY>0</floatingY>
  <noStrut>no</noStrut>
  <stacking>Above</stacking>
  <!-- 'Above', 'Normal', or 'Below' -->
  <direction>Vertical</direction>
  <!-- 'Vertical' or 'Horizontal' -->
  <autoHide>no</autoHide>
  <hideDelay>300</hideDelay>
  <!-- in milliseconds (1000 = 1 second) -->
  <showDelay>300</showDelay>
  <!-- in milliseconds (1000 = 1 second) -->
  <moveButton>Middle</moveButton>
  <!-- 'Left', 'Middle', 'Right' -->
</dock>

<keyboard>
  <chainQuitKey>C-g</chainQuitKey>

  <!-- Keybindings for desktop switching -->
  <keybind key="C-A-Left">
    <action name="GoToDesktop"><to>left</to><wrap>no</wrap></action>
  </keybind>
  <keybind key="C-A-Right">
    <action name="GoToDesktop"><to>right</to><wrap>no</wrap></action>
  </keybind>
  <keybind key="C-A-Up">
    <action name="GoToDesktop"><to>up</to><wrap>no</wrap></action>
  </keybind>
  <keybind key="C-A-Down">
    <action name="GoToDesktop"><to>down</to><wrap>no</wrap></action>
  </keybind>
  <keybind key="S-A-Left">
    <action name="SendToDesktop"><to>left</to><wrap>no</wrap></action>
  </keybind>
  <keybind key="S-A-Right">
    <action name="SendToDesktop"><to>right</to><wrap>no</wrap></action>
  </keybind>
  <keybind key="S-A-Up">
    <action name="SendToDesktop"><to>up</to><wrap>no</wrap></action>
  </keybind>
  <keybind key="S-A-Down">
    <action name="SendToDesktop"><to>down</to><wrap>no</wrap></action>
  </keybind>
  <keybind key="W-F1">
    <action name="GoToDesktop"><to>1</to></action>
  </keybind>
  <keybind key="W-F2">
    <action name="GoToDesktop"><to>2</to></action>
  </keybind>
  <keybind key="W-F3">
    <action name="GoToDesktop"><to>3</to></action>
  </keybind>
  <keybind key="W-F4">
    <action name="GoToDesktop"><to>4</to></action>
  </keybind>
  <keybind key="W-d">
    <action name="ToggleShowDesktop"/>
  </keybind>

  <!-- Keybindings for windows -->
  <keybind key="A-F4">
    <action name="Close"/>
  </keybind>
  <keybind key="A-Escape">
    <action name="Lower"/>
    <action name="FocusToBottom"/>
    <action name="Unfocus"/>
  </keybind>
  <keybind key="A-space">
    <action name="ShowMenu"><menu>client-menu</menu></action>
  </keybind>
  <!-- Take a screenshot of the current window with scrot when Alt+Print are pressed -->
  <keybind key="A-Print">
    <action name="Execute"><command>scrot -s</command></action>
  </keybind>

  <!-- Keybindings for window switching -->
  <keybind key="A-Tab">
    <action name="NextWindow">
      <finalactions>
        <action name="Focus"/>
        <action name="Raise"/>
        <action name="Unshade"/>
      </finalactions>
    </action>
  </keybind>
  <keybind key="A-S-Tab">
    <action name="PreviousWindow">
      <finalactions>
        <action name="Focus"/>
        <action name="Raise"/>
        <action name="Unshade"/>
      </finalactions>
    </action>
  </keybind>
  <keybind key="C-A-Tab">
    <action name="NextWindow">
      <panels>yes</panels><desktop>yes</desktop>
      <finalactions>
        <action name="Focus"/>
        <action name="Raise"/>
        <action name="Unshade"/>
      </finalactions>
    </action>
  </keybind>

  <!-- Keybindings for window switching with the arrow keys -->
  <keybind key="W-S-Right">
    <action name="DirectionalCycleWindows">
      <direction>right</direction>
    </action>
  </keybind>
  <keybind key="W-S-Left">
    <action name="DirectionalCycleWindows">
      <direction>left</direction>
    </action>
  </keybind>
  <keybind key="W-S-Up">
    <action name="DirectionalCycleWindows">
      <direction>up</direction>
    </action>
  </keybind>
  <keybind key="W-S-Down">
    <action name="DirectionalCycleWindows">
      <direction>down</direction>
    </action>
  </keybind>

  <!-- Keybindings for running applications -->
  <keybind key="W-e">
    <action name="Execute">
      <startupnotify>
        <enabled>true</enabled>
        <name>Konqueror</name>
      </startupnotify>
      <command>kfmclient openProfile filemanagement</command>
    </action>
  </keybind>
  <!-- Launch scrot when Print is pressed -->
  <keybind key="Print">
    <action name="Execute"><command>scrot</command></action>
  </keybind>
</keyboard>

<mouse>
  <dragThreshold>1</dragThreshold>
  <!-- number of pixels the mouse must move before a drag begins -->
  <doubleClickTime>500</doubleClickTime>
  <!-- in milliseconds (1000 = 1 second) -->
  <screenEdgeWarpTime>400</screenEdgeWarpTime>
  <!-- Time before changing desktops when the pointer touches the edge of the
       screen while moving a window, in milliseconds (1000 = 1 second).
       Set this to 0 to disable warping -->
  <screenEdgeWarpMouse>false</screenEdgeWarpMouse>
  <!-- Set this to TRUE to move the mouse pointer across the desktop when
       switching due to hitting the edge of the screen -->

  <context name="Frame">
    <mousebind button="A-Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
    </mousebind>
    <mousebind button="A-Left" action="Click">
      <action name="Unshade"/>
    </mousebind>
    <mousebind button="A-Left" action="Drag">
      <action name="Move"/>
    </mousebind>

    <mousebind button="A-Right" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="Unshade"/>
    </mousebind>
    <mousebind button="A-Right" action="Drag">
      <action name="Resize"/>
    </mousebind> 

    <mousebind button="A-Middle" action="Press">
      <action name="Lower"/>
      <action name="FocusToBottom"/>
      <action name="Unfocus"/>
    </mousebind>

    <mousebind button="A-Up" action="Click">
      <action name="GoToDesktop"><to>previous</to></action>
    </mousebind>
    <mousebind button="A-Down" action="Click">
      <action name="GoToDesktop"><to>next</to></action>
    </mousebind>
    <mousebind button="C-A-Up" action="Click">
      <action name="GoToDesktop"><to>previous</to></action>
    </mousebind>
    <mousebind button="C-A-Down" action="Click">
      <action name="GoToDesktop"><to>next</to></action>
    </mousebind>
    <mousebind button="A-S-Up" action="Click">
      <action name="SendToDesktop"><to>previous</to></action>
    </mousebind>
    <mousebind button="A-S-Down" action="Click">
      <action name="SendToDesktop"><to>next</to></action>
    </mousebind>
  </context>

  <context name="Titlebar">
    <mousebind button="Left" action="Drag">
      <action name="Move"/>
    </mousebind>
    <mousebind button="Left" action="DoubleClick">
      <action name="ToggleMaximize"/>
    </mousebind>

    <mousebind button="Up" action="Click">
      <action name="if">
        <shaded>no</shaded>
        <then>
          <action name="Shade"/>
          <action name="FocusToBottom"/>
          <action name="Unfocus"/>
          <action name="Lower"/>
        </then>
      </action>
    </mousebind>
    <mousebind button="Down" action="Click">
      <action name="if">
        <shaded>yes</shaded>
        <then>
          <action name="Unshade"/>
          <action name="Raise"/>
        </then>
      </action>
    </mousebind>
  </context>

  <context name="Titlebar Top Right Bottom Left TLCorner TRCorner BRCorner BLCorner">
    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="Unshade"/>
    </mousebind>

    <mousebind button="Middle" action="Press">
      <action name="Lower"/>
      <action name="FocusToBottom"/>
      <action name="Unfocus"/>
    </mousebind>

    <mousebind button="Right" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="ShowMenu"><menu>client-menu</menu></action>
    </mousebind>
  </context>

  <context name="Top">
    <mousebind button="Left" action="Drag">
      <action name="Resize"><edge>top</edge></action>
    </mousebind>
  </context>

  <context name="Left">
    <mousebind button="Left" action="Drag">
      <action name="Resize"><edge>left</edge></action>
    </mousebind>
  </context>

  <context name="Right">
    <mousebind button="Left" action="Drag">
      <action name="Resize"><edge>right</edge></action>
    </mousebind>
  </context>

  <context name="Bottom">
    <mousebind button="Left" action="Drag">
      <action name="Resize"><edge>bottom</edge></action>
    </mousebind>

    <mousebind button="Right" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="ShowMenu"><menu>client-menu</menu></action>
    </mousebind>
  </context>

  <context name="TRCorner BRCorner TLCorner BLCorner">
    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="Unshade"/>
    </mousebind>
    <mousebind button="Left" action="Drag">
      <action name="Resize"/>
    </mousebind>
  </context>

  <context name="Client">
    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
    </mousebind>
    <mousebind button="Middle" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
    </mousebind>
    <mousebind button="Right" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
    </mousebind>
  </context>

  <context name="Icon">
    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="Unshade"/>
      <action name="ShowMenu"><menu>client-menu</menu></action>
    </mousebind>
    <mousebind button="Right" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="ShowMenu"><menu>client-menu</menu></action>
    </mousebind>
  </context>

  <context name="AllDesktops">
    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="Unshade"/>
    </mousebind>
    <mousebind button="Left" action="Click">
      <action name="ToggleOmnipresent"/>
    </mousebind>
  </context>

  <context name="Shade">
    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
    </mousebind>
    <mousebind button="Left" action="Click">
      <action name="ToggleShade"/>
    </mousebind>
  </context>

  <context name="Iconify">
    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
    </mousebind>
    <mousebind button="Left" action="Click">
      <action name="Iconify"/>
    </mousebind>
  </context>

  <context name="Maximize">
    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="Unshade"/>
    </mousebind>
    <mousebind button="Middle" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="Unshade"/>
    </mousebind>
    <mousebind button="Right" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="Unshade"/>
    </mousebind>
    <mousebind button="Left" action="Click">
      <action name="ToggleMaximize"/>
    </mousebind>
    <mousebind button="Middle" action="Click">
      <action name="ToggleMaximize"><direction>vertical</direction></action>
    </mousebind>
    <mousebind button="Right" action="Click">
      <action name="ToggleMaximize"><direction>horizontal</direction></action>
    </mousebind>
  </context>

  <context name="Close">
    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
      <action name="Unshade"/>
    </mousebind>
    <mousebind button="Left" action="Click">
      <action name="Close"/>
    </mousebind>
  </context>

  <context name="Desktop">
    <mousebind button="Up" action="Click">
      <action name="GoToDesktop"><to>previous</to></action>
    </mousebind>
    <mousebind button="Down" action="Click">
      <action name="GoToDesktop"><to>next</to></action>
    </mousebind>

    <mousebind button="A-Up" action="Click">
      <action name="GoToDesktop"><to>previous</to></action>
    </mousebind>
    <mousebind button="A-Down" action="Click">
      <action name="GoToDesktop"><to>next</to></action>
    </mousebind>
    <mousebind button="C-A-Up" action="Click">
      <action name="GoToDesktop"><to>previous</to></action>
    </mousebind>
    <mousebind button="C-A-Down" action="Click">
      <action name="GoToDesktop"><to>next</to></action>
    </mousebind>

    <mousebind button="Left" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
    </mousebind>
    <mousebind button="Right" action="Press">
      <action name="Focus"/>
      <action name="Raise"/>
    </mousebind>
  </context>

  <context name="Root">
    <!-- Menus -->
    <mousebind button="Middle" action="Press">
      <action name="ShowMenu"><menu>client-list-combined-menu</menu></action>
    </mousebind> 
    <mousebind button="Right" action="Press">
      <action name="ShowMenu"><menu>root-menu</menu></action>
    </mousebind>
  </context>

  <context name="MoveResize">
    <mousebind button="Up" action="Click">
      <action name="GoToDesktop"><to>previous</to></action>
    </mousebind>
    <mousebind button="Down" action="Click">
      <action name="GoToDesktop"><to>next</to></action>
    </mousebind>
    <mousebind button="A-Up" action="Click">
      <action name="GoToDesktop"><to>previous</to></action>
    </mousebind>
    <mousebind button="A-Down" action="Click">
      <action name="GoToDesktop"><to>next</to></action>
    </mousebind>
  </context>
</mouse>

<menu>
  <!-- You can specify more than one menu file in here and they are all loaded,
       just don't make menu ids clash or, well, it'll be kind of pointless -->

  <!-- default menu file (or custom one in $HOME/.config/openbox/) -->
  <!-- system menu files on Debian systems -->
  <file>/var/lib/openbox/debian-menu.xml</file>
  <file>menu.xml</file>
  <hideDelay>200</hideDelay>
  <!-- if a press-release lasts longer than this setting (in milliseconds), the
       menu is hidden again -->
  <middle>no</middle>
  <!-- center submenus vertically about the parent entry -->
  <submenuShowDelay>100</submenuShowDelay>
  <!-- time to delay before showing a submenu after hovering over the parent
       entry.
       if this is a negative value, then the delay is infinite and the
       submenu will not be shown until it is clicked on -->
  <submenuHideDelay>400</submenuHideDelay>
  <!-- time to delay before hiding a submenu when selecting another
       entry in parent menu
       if this is a negative value, then the delay is infinite and the
       submenu will not be hidden until a different submenu is opened -->
  <showIcons>yes</showIcons>
  <!-- controls if icons appear in the client-list-(combined-)menu -->
  <manageDesktops>yes</manageDesktops>
  <!-- show the manage desktops section in the client-list-(combined-)menu -->
</menu>

<applications>
<!--
  # this is an example with comments through out. use these to make your
  # own rules, but without the comments of course.
  # you may use one or more of the name/class/role/title/type rules to specify
  # windows to match

  <application name="the window's _OB_APP_NAME property (see obxprop)"
              class="the window's _OB_APP_CLASS property (see obxprop)"
          groupname="the window's _OB_APP_GROUP_NAME property (see obxprop)"
         groupclass="the window's _OB_APP_GROUP_CLASS property (see obxprop)"
               role="the window's _OB_APP_ROLE property (see obxprop)"
              title="the window's _OB_APP_TITLE property (see obxprop)"
               type="the window's _OB_APP_TYPE property (see obxprob)..
                      (if unspecified, then it is 'dialog' for child windows)">
  # you may set only one of name/class/role/title/type, or you may use more
  # than one together to restrict your matches.

  # the name, class, role, and title use simple wildcard matching such as those
  # used by a shell. you can use * to match any characters and ? to match
  # any single character.

  # the type is one of: normal, dialog, splash, utility, menu, toolbar, dock,
  #    or desktop

  # when multiple rules match a window, they will all be applied, in the
  # order that they appear in this list


    # each rule element can be left out or set to 'default' to specify to not 
    # change that attribute of the window

    <decor>yes</decor>
    # enable or disable window decorations

    <shade>no</shade>
    # make the window shaded when it appears, or not

    <position force="no">
      # the position is only used if both an x and y coordinate are provided
      # (and not set to 'default')
      # when force is "yes", then the window will be placed here even if it
      # says you want it placed elsewhere.  this is to override buggy
      # applications who refuse to behave
      <x>center</x>
      # a number like 50, or 'center' to center on screen. use a negative number
      # to start from the right (or bottom for <y>), ie -50 is 50 pixels from
      # the right edge (or bottom). use 'default' to specify using value
      # provided by the application, or chosen by openbox, instead.
      <y>200</y>
      <monitor>1</monitor>
      # specifies the monitor in a xinerama setup.
      # 1 is the first head, or 'mouse' for wherever the mouse is
    </position>

    <size>
      # the size to make the window.
      <width>20</width>
      # a number like 20, or 'default' to use the size given by the application.
      # you can use fractions such as 1/2 or percentages such as 75% in which
      # case the value is relative to the size of the monitor that the window
      # appears on.
      <height>30%</height>
    </size>

    <focus>yes</focus>
    # if the window should try be given focus when it appears. if this is set
    # to yes it doesn't guarantee the window will be given focus. some
    # restrictions may apply, but Openbox will try to

    <desktop>1</desktop>
    # 1 is the first desktop, 'all' for all desktops

    <layer>normal</layer>
    # 'above', 'normal', or 'below'

    <iconic>no</iconic>
    # make the window iconified when it appears, or not

    <skip_pager>no</skip_pager>
    # asks to not be shown in pagers

    <skip_taskbar>no</skip_taskbar>
    # asks to not be shown in taskbars. window cycling actions will also
    # skip past such windows

    <fullscreen>yes</fullscreen>
    # make the window in fullscreen mode when it appears

    <maximized>true</maximized>
    # 'Horizontal', 'Vertical' or boolean (yes/no)
  </application>

  # end of the example
-->
</applications>

</openbox_config>

EOT


echo "Ustawianie tapety ..."
mkdir -pv $BACKDIR
cd $HOME"/Obrazy/"
$SUSER nitrogen HOME"/Obrazy/background.jpg"
wget --user-agent="ok" toromatic.pl/css/background.jpg
mv background.jpg $BACKDIR"tlo.jpg"
mkdir -pv $HOME"/.config/nitrogen"
cat <<EOT > $HOME"/.config/nitrogen/bg-saved.cfg"
[:0.0]
EOT
echo "file="$HOME"/Obrazy/tlo.jpg" >> $HOME"/.config/nitrogen/bg-saved.cfg"
cat <<EOT >> $HOME"/.config/nitrogen/bg-saved.cfg"
mode=4
bgcolor=#000000
EOT
echo 'nitrogen --restore &' >> $OBNDIR/"autostart"

ech "Zmina czasu wyswietlania gruba"
sed -i "/GRUB_TIMEOUT=5/c GRUB_TIMEOUT=0" /etc/default/grub 
update-grub

#$SUSER nitrogen $HOME"/Obrazy/tlo.jpg"

echo "----------------------------------------------"
echo "konfiguracja openboxa zakonczona"
echo "----------------------------------------------"
}


#-------- FUNKCJE POZOSTALE --------#
addusertosudo() #dodanie uzytkownika do grupy sudo
{
    adduser $USER sudo
}

configurepanelformadmin()
{
echo "----------------------------------------------"
echo "konfiguracja panelu dla roota"
echo "----------------------------------------------"

mkdir -pv /root/.config/lxpanel/default/panels
cd /root/.config/lxpanel/default

#komenda wylogowania 
cat <<EOT > "config"
[Command]
Logout=/sbin/shutdown -h now
EOT

#panel gorny
cd /root/.config/lxpanel/default/panels
cat <<EOT > "up"
# lxpanel <profile> config file. Manually editing is not recommended.
# Use preference dialog in lxpanel to adjust config when you can.

Global {
  edge=top
  monitor=0
  transparent=1
  background=0
  tintcolor=#000000
  alpha=255
  usefontcolor=1
  fontcolor=#ffffff
  usefontsize=0
}
Plugin {
  type=menu
  Config {
    system {
    }
    separator {
    }
    item {
      command=run
    }
    separator {
    }
    item {
      command=logout
      image=gnome-logout
    }
    image=/usr/share/lxpanel/images/my-computer.png
  }
}
Plugin {
  type=dirmenu
  Config {
    path=/home/user
  }
}
Plugin {
  type=launchbar
  Config {
    Button {
      id=menu://applications/System/gnome-terminal.desktop
    }
    Button {
      id=menu://applications/Development/geany.desktop
    }
  }
}
Plugin {
  type=space
  Config {
    Size=2
  }
  expand=1
}
Plugin {
  type=tray
  Config {
  }
}
Plugin {
  type=volumealsa
  Config {
  }
}
Plugin {
  type=wincmd
  Config {
  }
}
Plugin {
  type=dclock
  Config {
    ClockFmt=%R %d.%m.%Y
    TooltipFmt=%A %x
    BoldFont=1
    IconOnly=0
    CenterText=1
  }
}

EOT

#panel dolny
cd /root/.config/lxpanel/default/panels
cat <<EOT > "down"
# lxpanel <profile> config file. Manually editing is not recommended.
# Use preference dialog in lxpanel to adjust config when you can.

Global {
  edge=bottom
  monitor=0
  transparent=1
  background=0
  tintcolor=#000000
  alpha=255
}
Plugin {
  type=taskbar
  Config {
  }
}
Plugin {
  type=pager
  Config {
  }
}

EOT

#dodanie rzeczy do autostartu
mkdir -pv /root/.config/openbox/
cd /root/.config/openbox
pwd
cat <<EOT > "autostart"
#pasek na dole i na gorze
lxpanel &
EOT

echo "----------------------------------------------"
echo "konfiguracja panelu dla roota zakonczona"
echo "----------------------------------------------"

}

#-------- MAIN --------#
if [ $# = 0 ]
then
  #installrepository
  #installpackages
  #installkernel
  #installlinuxcnc
  setautologin
  configuserspace
  addusertosudo
    configurepanelformadmin
    exit 0
    
else
  while getopts :rpkcauhl opt; do
    case $opt in
      r) echo "instalacja repozytoriow"
         installrepository
         ;;
      p) echo "instalacja pakietow"
         installpackages
         ;;
      k) echo "instalacja jadra"
         installkernel
         ;;           
      c) echo "instalacja linuxcnc"
         installlinuxcnc
         ;;
      a) echo "autologowanie"
         setautologin
         ;;           
      u) echo "konfiguracja konta uzytkownika"
         configuserspace
         ;;     
            l) echo "konfiguracja panelu dla roota"
         configurepanelformadmin
         ;;                 
      h) echo "help"
         usage
         exit 1
         ;;           
    esac
  done
fi
